<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login | Dashboard JCO R</title>
    <!-- Font Family Poppins -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600&display=swap" rel="stylesheet">
    <!-- Local style -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/login.css">
    <link rel="stylesheet" href="<?= URL_LAYOUT ?>assets/css/flexbox.css">
    <link rel="stylesheet" href="<?= URL_LAYOUT ?>assets/css/sweetalert2-dark.min.css">
    <!-- Icon brand -->
    <link rel="shortcut icon" href="<?= URL_LAYOUT ?>assets/img/jco-r.ico" type="image/x-icon">
    <link rel="icon" href="<?= URL_LAYOUT ?>assets/img/jco-r.ico" type="image/x-icon">
</head>
<body>
    <!-- Loader -->
    <div class="loader">
        <img src="<?= base_url(); ?>assets/img/loader.svg">
    </div>
    <div class="row">
        <div class="col-xs-12 col-lg-6">
            <div class="login__left">
                <img src="<?= URL_LAYOUT ?>assets/img/logo_jco-r_primary.png" alt="Brand logo">
            </div>
        </div>
        
        <div class="col-xs-12 col-lg-6 middle-md">
            <div class="login__right">
                <div class="form__wrapper">
                    <p class="text-muted">Welcome to</p>
                    <h1>J.CO Reserve</h1>
                    <form class="form__login">
                        <!-- CSRF token -->
                        <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>">
                        <!-- Username -->
                        <input type="text" name="user-name" placeholder="Username" autofocus required>

                        <!-- Password -->
                        <input type="password" name="user-pass" placeholder="Password" autocomplete="off" required>

                        <!-- Submit -->
                        <input type="submit" value="Login" class="btn__login">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="<?= base_url() ?>global.js"></script>
    <script src="<?= URL_LAYOUT ?>assets/js/jquery.js"></script>
    <script src="<?= URL_LAYOUT ?>assets/js/sweetalert2.min.js"></script>
    <script>
        $(window).on('load', () => {
            $(".loader").hide();
        });

        // formLogin.addEventListener('submit', (evt) => {
        $('.form__login').on('submit', (evt) => {
            evt.preventDefault();
            const formData = new FormData(evt.target);
            $(".loader").show();
            try{
                fetch(`${base_url}check_login`, {
                    method: 'POST',
                    body: formData
                })
                .then((response) => {
                    // Check response fetch is ok
                    if(response.ok) return response.json(); 
                    
                    // Return promise reject 
                    return Promise.reject(response);
                })
                .then((data) => {
                    $(".loader").hide();
                    $('input[name="<?= $this->security->get_csrf_token_name(); ?>"]').val(data.refresh_csrf_token);
                    console.log(data);

                    if(data.success){
                        Swal.fire({
                            icon: 'success',
                            title: 'Login Success',
                            text: `${data.msg}`,
                            showConfirmButton: false, 
                            showCancelButton: false,
                            timer: 2000
                        }).then(() => {
                            // Redirect to main dashboard page
                            window.location.href = `${base_url}`;
                        });
                    } else {
                        Swal.fire({
                            icon: 'warning',
                            title: 'Oops...',
                            html: `${data.msg}`,
                            showConfirmButton: true, 
                            showCancelButton: false,
                        }); 
                    }
                })
                .catch((err)=>{console.error(err)});
            } catch(err){console.error(err)}
        });
    </script>
</body>
</html>