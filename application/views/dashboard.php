<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard JCO R</title>
    <!-- CSS Styling -->
    <link rel="stylesheet" href="<?= URL_LAYOUT ?>assets/css/style.css">
    <link rel="stylesheet" href="<?= URL_LAYOUT ?>assets/css/flexbox.css">
    <link rel="stylesheet" href="<?= URL_LAYOUT ?>assets/css/daterangepicker.css" />
    <link rel="stylesheet" href="<?= URL_LAYOUT ?>assets/css/select2.css" />
    <link rel="stylesheet" href="<?= URL_LAYOUT ?>assets/css/sweetalert2-dark.min.css">
    <link rel="stylesheet" href="https://unpkg.com/filepond/dist/filepond.css" >
    <link rel="stylesheet" href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.0/ui/trumbowyg.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.0/plugins/colors/ui/trumbowyg.colors.min.css"/>
    <link rel="stylesheet" href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css"/>
    <!-- Icon brand -->
    <link rel="shortcut icon" href="<?= URL_LAYOUT ?>assets/img/jco-r.ico" type="image/x-icon">
    <link rel="icon" href="<?= URL_LAYOUT ?>assets/img/jco-r.ico" type="image/x-icon">
    <!-- Font Family Poppins -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600&display=swap" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/9d123f77ce.js" crossorigin="anonymous"></script>
    <style>
        ::-webkit-calendar-picker-indicator {
            filter: invert(1);
        }
        .choosen-container{
            width: 100% !important;
            font-family: "Poppins", sans-serif, -apple-system, BlinkMacSystemFont;
        }
        .chosen-container-multi .chosen-choices{
            font-family: "Poppins", sans-serif, -apple-system, BlinkMacSystemFont;
            background-color: #131313 !important;
            background-image: linear-gradient(90deg, rgba(19,19,19,1) 0%, rgba(19,19,19,1) 100%) !important;
            background-image: -webkit-linear-gradient(90deg, rgba(19,19,19,1) 0%, rgba(19,19,19,1) 100%) !important;
            color: white !important;
            border: solid 2px #131313;
            padding: 0.6rem 0.9rem !important;
        }
        .chosen-container-multi .chosen-choices li.search-choice{
            background-color: #87764E !important;
            /* background-image: linear-gradient(90deg, rgba(135,118,78,1) 0%, rgba(135,118,78,1) 100%) !important; */
            /* background-image: -webkit-linear-gradient(90deg, rgba(135,118,78,1) 0%, rgba(135,118,78,1) 100%) !important; */
            background-image: linear-gradient(90deg, rgba(19,19,19,1) 0%, rgba(19,19,19,1) 100%) !important;
            background-image: -webkit-linear-gradient(90deg, rgba(19,19,19,1) 0%, rgba(19,19,19,1) 100%) !important;
            border: solid 1px #87764E !important;
            color: white !important;
        }
        .chosen-container-multi .chosen-choices li.search-field input[type=text]{
            font-family: "Poppins", sans-serif, -apple-system, BlinkMacSystemFont;
            font-size: 15px !important;
        }
        .chosen-container-multi .chosen-choices li.search-choice .search-choice-close{
            filter: invert(100%) sepia(100%) saturate(1%) hue-rotate(100deg) brightness(109%) contrast(101%) !important;
        }
        /* Background date picker */
        .daterangepicker,
        .calendar-table{
            background: #0D0D0D !important;
        }

        /* Date active */
        .daterangepicker td.active, .daterangepicker td.active:hover,
        .daterangepicker td.end-date, .daterangepicker td.end-date:hover{
            background-color: #87764E !important;
            color: white !important;
        }

        /* border calendar table */
        .daterangepicker{
            border: solid 1.5px #87764E;
            border-radius: 10px;
        }
        .calendar-table{
            border: none !important;
        }

        /* Font color months */
        .daterangepicker .calendar-table thead{
            color: white !important;
        }
        .daterangepicker .calendar-table tbody{
            background-color: transparent;
        }
        /* Font color date */
        .daterangepicker td.off, .daterangepicker td.off.in-range{
            background-color: transparent;
        }
        .daterangepicker td{
            color: #87764E;
        }
        .daterangepicker td.in-range{
            color: #87764E;
            background-color: rgba(135, 118, 78, .2);
        }
        .daterangepicker td.available:hover, .daterangepicker th.available:hover{
            background-color: rgba(135, 118, 78, .2);
            color: #87764E;
        }

        /* btn daterangepicker */
        .daterangepicker .drp-buttons {
            border-top: 2px solid #303030;
        }
        .daterangepicker .drp-selected{
            color: white;
        }
        .daterangepicker .btn-default{
            background-color: transparent;
            border: solid 1px #87764E;
            color: #87764E;
        }
        .daterangepicker .btn-primary{
            border: solid 1px #87764E;
            background-color: #87764E;
            color: white;
        }

        /* button prev and next  */
        .daterangepicker .calendar-table .next span, .daterangepicker .calendar-table .prev span {
            border: solid white;
            border-width: 0 2px 2px 0;
        }
    </style>
</head>
<body>
    <div class="loader">
        <img src="<?= base_url(); ?>assets/img/loader.svg">
    </div>

    <div class="main" data-brand_id="<?= encrypt(2); ?>" data-brand_name="<?= $this->session->userdata('main_brand_name'); ?>" data-admin_name="<?= $this->session->userdata('main_admin_name'); ?>" data-admin_token="<?= $this->session->userdata('main_admin_token'); ?>">
        <?php print_r($this->session->userdata()); ?>
    </div>

    <script src="<?= URL_LAYOUT ?>assets/js/jquery.js"></script>
    <script src="<?= URL_LAYOUT ?>assets/js/sweetalert2.min.js"></script>
    <script src="<?= URL_LAYOUT ?>assets/js/moment.min.js"></script>
    <script src="<?= URL_LAYOUT ?>assets/js/daterangepicker.js"></script>
    <script src="<?= URL_LAYOUT ?>assets/js/select2.min.js"></script>
    <script src="<?= URL_LAYOUT ?>global.js"></script>
    <script src="<?= URL_LAYOUT ?>assets/js/script.js"></script>
    <script src="https://unpkg.com/filepond/dist/filepond.min.js"></script>
    <script src="https://unpkg.com/jquery-filepond/filepond.jquery.js"></script>
    <script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.js"></script>
    <script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
    <script src="https://unpkg.com/filepond-plugin-image-validate-size/dist/filepond-plugin-image-validate-size.js"></script>
    <script src="https://unpkg.com/filepond-plugin-image-crop/dist/filepond-plugin-image-crop.js"></script>
    <script src="https://unpkg.com/filepond-plugin-file-encode/dist/filepond-plugin-file-encode.js"></script>
    <script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
    <script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.0/trumbowyg.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.0/plugins/fontfamily/trumbowyg.fontfamily.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.0/plugins/colors/trumbowyg.colors.min.js"></script>
</body>
</html>