<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400000');
}

class Main_controller extends CI_Controller {

	public function index(){
        if($this->session->userdata('main_brand_id') > 0){
            $this->load->view('dashboard');
        } else {
            redirect(base_url() . 'log-in');
        }
	}

    public function login(){
        $this->load->view('login');
    }

    public function check_login(){
        // Set cofnig for from
        $config_login = array(
            // Config username
            array(
                'field' => 'user-name',
                'label' => 'username',
                'rules' => 'trim|required|alpha_dash',
                'errors' => array(
                    'required' => 'Please fill out %s field',
                    'alpa_dash' => 'Input must contains anything other than alpha-numeric characters, underscores or dashes.'
                ),
            ),
            // Config password
            array(
                'field' => 'user-pass',
                'label' => 'password',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Please fill out %s field'
                )
            ),
        );
        // Set rules config for form
		$this->form_validation->set_rules($config_login);

        // Check validation config form
        if($this->form_validation->run() != FALSE){
            // Initialize data from login
            $data_json = array(
                "username" => htmlentities($this->input->post('user-name')),
                "password" => htmlentities($this->input->post('user-pass'))
            );

            $return = login(json_encode($data_json));
            $result = json_decode($return, true);

            if($result['status_code'] == 200){
                $this->session->set_userdata('main_brand_id', 2);
                $this->session->set_userdata('main_brand_name', 'jco-r');
                $this->session->set_userdata('main_admin_name', $result['data']['admin']['username']);
                $this->session->set_userdata('main_admin_token', $result['data']['token']['accessToken']);

                $data_result = array(
                    "success" => true,
                    "msg" => "Please wait until the next page"
                );
            } else {
                $data_result = array(
                    "success" => false,
                    "msg" => "Invalid username or password"
                );
            }
            
        } else {
            $data_result = array(
                "success" => false,
                "msg" => validation_errors('', '')
            );
        }

        $data_result['refresh_csrf_token'] = $this->security->get_csrf_hash();

        echo json_encode($data_result);
    }

    public function logout(){
        $this->session->sess_destroy();
        
        redirect(base_url());
	}
}
