<?php defined('BASEPATH') OR exit('No direct script access allowed');

function encrypt($data) {
    $data = $data * 92471598 + 3;
    return($data);
}

function decrypt($data) {
    $data = ($data - 3) / 92471598;
    return($data);
}

function login($data_json){
    $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL             => API_URL . 'pos/login',
            CURLOPT_POST            => 1,
            CURLOPT_POSTFIELDS      => $data_json,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_HTTPHEADER      => [
                'Content-Type: application/json',
                'Authorization: ' . APP_TOKEN
            ]
        ]);

    $output = curl_exec($ch);
    curl_close($ch);

    return $output;
}